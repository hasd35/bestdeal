package com.example.bestdeal.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;


import com.example.bestdeal.object.Product;
import com.example.bestdeal.contract.ProductContract;
import com.example.bestdeal.helper.AppDBHelper;

import java.util.ArrayList;

public class ProductDAO {

    private final Context mContext;
    private final SQLiteDatabase db;


    public ProductDAO(Context context) {
        this.mContext = context;
        AppDBHelper dbHelper = new AppDBHelper(context);
        db = dbHelper.getWritableDatabase();
    }


    public ArrayList<Product> getProductos() {
        Product producto;
        ArrayList<Product> productoList = new ArrayList<>();
        Cursor dbCursor;

        String[] columnasSeleccionadas = {
                ProductContract.FIELD_NAME_CODIGO,
                ProductContract.FIELD_NAME_NOMBRE,
                ProductContract.FIELD_NAME_DESCRIPCION,
                ProductContract.FIELD_NAME_CATEGORIA,
                ProductContract.FIELD_NAME_PRECIO,
                ProductContract.FIELD_NAME_STOCK,
                ProductContract.FIELD_NAME_IMAGEN
        };


        dbCursor = db.query(
                ProductContract.TABLE_NAME_PRODUCTOS,
                columnasSeleccionadas,
                  null,
                null,
                null,
                null,
                null
        );

        while(dbCursor.moveToNext()){

            producto = new Product();
            producto = new Product();
            producto.setCodigo(dbCursor.getInt(0));
            producto.setNombre(dbCursor.getString(1));
            producto.setDescripcion(dbCursor.getString(2));
            producto.setCategoria(dbCursor.getString(3));
            producto.setPrecio(dbCursor.getLong(4));
            producto.setStock(dbCursor.getInt(5));
            producto.setImagen(dbCursor.getString(6));
            productoList.add(producto);
            productoList.add(producto);
        }

        return productoList;
    }



    public void guardarProducto (Product producto){
        SQLiteStatement statement;

        db.beginTransactionNonExclusive();
        statement = db.compileStatement("INSERT INTO productos (codigo,nombre,descripcion,precio,fabricante,imagen,cantidad,categoria,referencia,breveDesc) VALUES (?,?,?,?,?,?,?,?,?,?)");
        statement.bindLong(0,producto.getCodigo());
        statement.bindString(1,producto.getNombre());
        statement.bindString(2,producto.getDescripcion());
        statement.bindLong(3,producto.getPrecio());
        statement.bindString(5,producto.getImagen());
        statement.bindLong(6,producto.getStock());
        statement.bindString(7,producto.getCategoria());

        statement.executeInsert();

        db.setTransactionSuccessful();
        db.endTransaction();
    }

    public void guardarProducto2 (Product producto){
        ContentValues values = new ContentValues();

        values.put(ProductContract.FIELD_NAME_CODIGO,producto.getCodigo());
        values.put(ProductContract.FIELD_NAME_NOMBRE,producto.getNombre());
        values.put(ProductContract.FIELD_NAME_DESCRIPCION,producto.getDescripcion());
        values.put(ProductContract.FIELD_NAME_PRECIO,producto.getPrecio());
        values.put(ProductContract.FIELD_NAME_STOCK,producto.getStock());
        values.put(ProductContract.FIELD_NAME_IMAGEN,producto.getImagen());

        db.insert(ProductContract.TABLE_NAME_PRODUCTOS, null,values);
    }
}
