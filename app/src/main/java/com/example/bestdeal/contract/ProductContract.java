package com.example.bestdeal.contract;

import android.provider.BaseColumns;

public class ProductContract implements BaseColumns {

     public static final String TABLE_NAME_PRODUCTOS = "productos";
     public static final String FIELD_NAME_CODIGO = "ID";
     public static final String FIELD_NAME_NOMBRE = "NOMBRE";
     public static final String FIELD_NAME_DESCRIPCION = "DESCRIPCION";
     public static final String FIELD_NAME_CATEGORIA = "CATEGORIA";
     public static final String FIELD_NAME_PRECIO = "PRECIO";
     public static final String FIELD_NAME_STOCK = "STOCK";
     public static final String FIELD_NAME_IMAGEN = "IMAGEN";

    public static final String SQL_CREATE_TABLE = "CREATE TABLE " + TABLE_NAME_PRODUCTOS + " (" +
            FIELD_NAME_CODIGO + " INTEGER PRIMARY KEY , " +
            FIELD_NAME_NOMBRE + " TEXT NO NULL, " +
            FIELD_NAME_DESCRIPCION + " TEXT , " +
            FIELD_NAME_CATEGORIA + " TEXT , "+
            FIELD_NAME_PRECIO + " INTEGER, " +
            FIELD_NAME_STOCK + " INTEGER, " +
            FIELD_NAME_IMAGEN + " TEXT , "+");";

    public static final String SQL_DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME_PRODUCTOS;

    private ProductContract() {
    }

}
