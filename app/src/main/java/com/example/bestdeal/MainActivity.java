package com.example.bestdeal;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.Toast;

import com.example.bestdeal.object.Product;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    ArrayList<Product> listProducts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);


        SharedPreferences sesionPref = this.getSharedPreferences("sesionPref", Context.MODE_PRIVATE);

        Log.i(MainActivity.class.getSimpleName(), "onCreate: "+sesionPref.getAll().toString());
        if(!sesionPref.getBoolean("SesionIniciada",false)){
            SharedPreferences.Editor editPref = sesionPref.edit();
            editPref.putBoolean("SesionIniciada",true);
            editPref.apply();
        }
        loadData();

        //initUI();


    }

    private void loadData() {
        Toast.makeText(this, "Se carga por primera vez toda la data", Toast.LENGTH_SHORT).show();

        listProducts = new ArrayList<>();
        listProducts.add(new Product(3459499,"Huawei P20 128GB","El HUAWEI P20 abre un nuevo camino en cuanto a la fotografía de los smartphones presentando la nueva cámara dual Leica.","Smartphones", (long) 1399900,12));
        listProducts.add(new Product(3493329,"Moto G6","Con un diseño único y sofisticado, moto G6 tiene un acabado trasero de vidrio 3D con un efecto visual innovador que lo hace aún más elegante.","Smartphones", (long) 399900,2));


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_cart) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();



        if (id == R.id.nav_home) {

        } else if (id == R.id.nav_cart) {

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
